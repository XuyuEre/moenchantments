package com.biom4st3r.moenchantments;

public class MoEnchantsConfig
{
    public boolean EnableAutoSmelt ;
    public boolean EnableEnderProtection ;
    public boolean EnablePotionRetention ;
    public boolean EnableTamedProtection ;
    public boolean EnableTreeFeller ;
    public boolean EnableVeinMiner ;

    public int[] VeinMinerMaxBreakByLvl;

    public int[] TreeFellerMaxBreakByLvl;
    
    public int MaxDistanceFromPlayer;

    public boolean ProtectItemFromBreaking;

    public float AutoSmeltWoodModifier;

    public boolean TameProtectsOnlyYourAnimals;

    //public boolean vanillaToolTips = false;

    public int chanceForEnderCurseToTeleport;
    public int chanceForEnderCurseToPreventDamage;

    public int perLevelChargeMultiplierForPotionRetention;
    public int PotionRetentionMaxLevel;

    public String[] veinMinerBlockWhiteList;
    public String[] veinMinerClassWhiteList;

    public MoEnchantsConfig()
    {
        VeinMinerMaxBreakByLvl = new int[] {7,14,28};
        TreeFellerMaxBreakByLvl = new int[] {7,14,28};
        MaxDistanceFromPlayer = 8;
        ProtectItemFromBreaking = true;

        AutoSmeltWoodModifier = 0.15f;
        TameProtectsOnlyYourAnimals = true;
        chanceForEnderCurseToPreventDamage = 20;
        chanceForEnderCurseToTeleport = 40;
        perLevelChargeMultiplierForPotionRetention = 5;
        PotionRetentionMaxLevel = 10;
        veinMinerBlockWhiteList = new String[] {
            "minecraft:obsidian"
        };
        veinMinerClassWhiteList = new String[] {
            "com.brand.netherthings.blocks.BlockOre",
            "com.brand.netherthings.blocks.BlockGlowOre"
        };
        EnableAutoSmelt = true;
        EnableEnderProtection = true;
        EnablePotionRetention = true;
        EnableTamedProtection = true;
        EnableTreeFeller = true;
        EnableVeinMiner = true;
    }




    






}