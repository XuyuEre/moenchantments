package com.biom4st3r.moenchantments;

import com.biom4st3r.moenchantments.Enchantments.AutoSmeltEnchant;
import com.biom4st3r.moenchantments.Enchantments.EnderProtectionEnchant;
import com.biom4st3r.moenchantments.Enchantments.HavestingEnchantment;
import com.biom4st3r.moenchantments.Enchantments.HerosSwordEnchant;
import com.biom4st3r.moenchantments.Enchantments.MoEnchant;
import com.biom4st3r.moenchantments.Enchantments.PotionRetentionEnchant;
import com.biom4st3r.moenchantments.Enchantments.SentienceEnchant;
import com.biom4st3r.moenchantments.Enchantments.TamedProtectionEnchant;
import com.biom4st3r.moenchantments.Enchantments.TreeFellerEnchant;
import com.biom4st3r.moenchantments.Enchantments.VeinMinerEnchant;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class MoEnchants
{
    public static final String MODID = MoEnchantmentsMod.MODID;

    public static final TreeFellerEnchant TREEFELLER = new TreeFellerEnchant();
	public static final VeinMinerEnchant VEINMINER = new VeinMinerEnchant();
	public static final AutoSmeltEnchant AUTOSMELT = new AutoSmeltEnchant();
	public static final TamedProtectionEnchant TAMEDPROTECTION = new TamedProtectionEnchant();
	public static final EnderProtectionEnchant ENDERPROTECTION = new EnderProtectionEnchant();
    public static final HavestingEnchantment HAVESTER = new HavestingEnchantment();
    public static final HerosSwordEnchant HEROSSWORD = new HerosSwordEnchant();
    public static final SentienceEnchant SENTIENCE = new SentienceEnchant();
    public static final PotionRetentionEnchant POTIONRETENTION = new PotionRetentionEnchant();

    public static void init()
    {
        //MinecraftClient
        //ClientPlayerEntity
        //ClientPlayerInteractionManager
        //PlayerEntity
        for(MoEnchant e : new MoEnchant[] {TREEFELLER,VEINMINER,AUTOSMELT,TAMEDPROTECTION,ENDERPROTECTION,POTIONRETENTION, HAVESTER, HEROSSWORD,SENTIENCE})
        {
            if(e.enabled())
                Registry.register(Registry.ENCHANTMENT, new Identifier(MODID, e.regName().toLowerCase()), e);
            
        }
    }
}