package com.biom4st3r.moenchantments.mixin;

import java.util.Map;
import com.biom4st3r.moenchantments.MoEnchantmentsMod;
import com.biom4st3r.moenchantments.MoEnchants;
import com.biom4st3r.moenchantments.util;
import com.biom4st3r.moenchantments.Enchantments.MoEnchant;
import com.biom4st3r.moenchantments.Logic.EnderProtectionLogic;
import com.biom4st3r.moenchantments.interfaces.PotionRetentionTarget;
import com.google.common.collect.Maps;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin extends Entity implements PotionRetentionTarget {
    protected LivingEntityMixin(EntityType<?> entityType_1, World world_1) {
        super(entityType_1, world_1); 
    }

    @Inject(at = @At("HEAD"), method = "damage", cancellable = true)
    public void damage(DamageSource damageSource_1, float float_1, CallbackInfoReturnable<Boolean> ci) 
    {
        if (((Entity) this) instanceof TameableEntity && damageSource_1.getAttacker() instanceof PlayerEntity) 
        {
            PlayerEntity attacker = (PlayerEntity) damageSource_1.getAttacker();
            TameableEntity defender = (TameableEntity) (Entity) this;
            if (MoEnchant.hasEnchant(MoEnchants.TAMEDPROTECTION, attacker.getMainHandStack())
                    && defender.getOwnerUuid() != MoEnchantmentsMod.uuidZero) 
                    {
                if (MoEnchantmentsMod.config.TameProtectsOnlyYourAnimals) 
                {
                    if (defender.isOwner(attacker)) {
                        ci.setReturnValue(false);
                    }
                } else 
                {
                    ci.setReturnValue(false);
                }
            }
        }
        int enderProLvl = EnchantmentHelper.getEquipmentLevel(MoEnchants.ENDERPROTECTION,
                ((LivingEntity) (Object) this));
        if (enderProLvl > 0) {
            if (EnderProtectionLogic.doLogic(damageSource_1, enderProLvl, (LivingEntity) (Object) this)) {
                ci.setReturnValue(false);
            }
        }
    }

    @Inject(at = @At("HEAD"), method = "addPotionEffect", cancellable = true)
    public void addPotionEffect(StatusEffectInstance statuseffect, CallbackInfoReturnable<Boolean> ci) {
        if(!this.world.isClient())
        {
            util.Debug("LivingEntityMixin#Inject_addPotionEffect",statuseffect.getDuration());
            
        }
    }

    @Shadow
    protected void method_6020(StatusEffectInstance statusEffectInstance_1){}

    @Shadow
    protected void method_6009(StatusEffectInstance statusEffectInstance_1, boolean boolean_1) {}

    @Shadow
    private final Map<StatusEffect, StatusEffectInstance> activeStatusEffects = Maps.newHashMap(); 

    @Shadow
    public boolean isPotionEffective(StatusEffectInstance sei){return true;}

    @Override
    public boolean applyRetainedPotionEffect(StatusEffectInstance newStatusEffect) 
    {
        if (!this.isPotionEffective(newStatusEffect)) {
            return false;
         } else {
            StatusEffectInstance foundStatusEffect = (StatusEffectInstance)this.activeStatusEffects.get(newStatusEffect.getEffectType());
            if (foundStatusEffect == null) { // if not in there
               this.activeStatusEffects.put(newStatusEffect.getEffectType(), newStatusEffect);
               this.method_6020(newStatusEffect);
               util.Debug("LivingEntityMixin#applyRetainedPotionEffect", "adding new effect to activestatuseffects Map");
               return true;
            } else if (foundStatusEffect.getEffectType() == newStatusEffect.getEffectType() && foundStatusEffect.getAmplifier() == newStatusEffect.getAmplifier()) { //if has instance of
                foundStatusEffect = new StatusEffectInstance(foundStatusEffect.getEffectType(),foundStatusEffect.getDuration()+newStatusEffect.getDuration(),foundStatusEffect.getAmplifier());
                this.activeStatusEffects.replace(foundStatusEffect.getEffectType(), foundStatusEffect);
                this.method_6020(foundStatusEffect);
                util.Debug("LivingEntityMixin#applyRetainedPotionEffect","increased duration of effect in activestatuseffects map");
                return true;
            } else { // 
               return false;
            }
         }    
    }
}