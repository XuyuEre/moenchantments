package com.biom4st3r.moenchantments.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import com.biom4st3r.moenchantments.Logic.onBreakMixinLogic;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

@Mixin(Block.class)
public class VeinMinerMixin
{

    @Inject(at = @At("HEAD"),method = "onBreak",cancellable = false)
    public void afterBreak(World world, BlockPos blockPos, BlockState blockState, PlayerEntity pe,CallbackInfo ci)
    {
        onBreakMixinLogic.tryVeinMining(world, blockPos, blockState, pe);
    }
}