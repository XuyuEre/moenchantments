package com.biom4st3r.moenchantments.mixin;

import com.biom4st3r.moenchantments.MoEnchants;
import com.biom4st3r.moenchantments.Logic.EnderProtectionLogic;
import com.biom4st3r.moenchantments.interfaces.PotionEffectRetainer;
import com.biom4st3r.moenchantments.interfaces.PotionRetentionTarget;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin extends LivingEntity {
    public PlayerEntityMixin(EntityType<? extends LivingEntity> entityType_1, World world_1) {
        super(entityType_1, world_1);
    }

    @Inject(at = @At("HEAD"), method = "applyDamage", cancellable = true)
    public void applyDamage(DamageSource damageSource, float float_1, CallbackInfo ci) {
        LivingEntity defender = (LivingEntity) (Object) this;
        int EnderProtectionLevel = EnchantmentHelper.getEquipmentLevel(MoEnchants.ENDERPROTECTION, defender);
        if (EnderProtectionLevel > 0) {
            if (EnderProtectionLogic.doLogic(damageSource, EnderProtectionLevel, defender)) 
            {
                ci.cancel();
            }
        }
    }

    @Inject(at = @At("HEAD"),method = "attack")
    public void attack(Entity defender,CallbackInfo ci)
    {
        PotionEffectRetainer stack = ((PotionEffectRetainer) (Object) this.getMainHandStack());
        if(MoEnchants.POTIONRETENTION.isAcceptableItem(this.getMainHandStack()) && stack.getCharges() > 0)
        {
            StatusEffectInstance effect = stack.useEffect();
            if(defender instanceof LivingEntity)
            {
                
                ((PotionRetentionTarget)defender).applyRetainedPotionEffect(effect);
            }
        }
    }
    // @Inject(at = @At("RETURN"),method = "getArrowType",cancellable = true)
    // public void getArrowType(ItemStack itemStack_1,CallbackInfoReturnable<ItemStack> ci)
    // {
    //     if(ci.getReturnValue().getItem() == Items.ARROW)
    //     {
    //         PotionEffectRetainer bow = ((PotionEffectRetainer) (Object) itemStack_1);
    //         if(bow.isPotionRetainer() > 0 && bow.getCharges() > 0)
    //         {
    //             Potion potion = new Potion(bow.useEffect());
    //             ItemStack arrow = ci.getReturnValue();
                
    //             ci.setReturnValue(PotionUtil.setPotion(arrow, potion));
    //             //PotionUtil.setPotion(,PotionUtil.itemstack);                
    //         }
    //     }
    // }
}