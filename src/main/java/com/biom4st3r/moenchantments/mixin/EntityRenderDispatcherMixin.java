// package com.biom4st3r.moenchantments.mixin;

// import com.biom4st3r.moenchantments.entities.HerosProjectileEntity;
// import org.spongepowered.asm.mixin.Mixin;
// import org.spongepowered.asm.mixin.Shadow;
// import org.spongepowered.asm.mixin.injection.At;
// import org.spongepowered.asm.mixin.injection.Inject;
// import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
// import net.minecraft.client.render.entity.EntityRenderDispatcher;
// import net.minecraft.client.render.entity.EntityRenderer;
// import net.minecraft.client.render.entity.FlyingItemEntityRenderer;
// import net.minecraft.client.render.item.ItemRenderer;
// import net.minecraft.client.texture.TextureManager;
// import net.minecraft.entity.Entity;
// import net.minecraft.resource.ReloadableResourceManager;

// @Mixin(EntityRenderDispatcher.class)
// public class EntityRenderDispatcherMixin
// {
//     @Shadow
//     private <T extends Entity> void register(Class<T> class_1, EntityRenderer<? super T> entityRenderer_1){}


//     @Inject(at=@At("RETURN"), method = "<init>")
//     public void onConstructed(TextureManager textureManager_1, ItemRenderer itemRenderer_1, ReloadableResourceManager reloadableResourceManager_1,CallbackInfo ci)
//     {
//         System.out.println("Registered");
//         this.register(HerosProjectileEntity.class, new FlyingItemEntityRenderer<HerosProjectileEntity>((EntityRenderDispatcher)(Object)this, itemRenderer_1,1f));
//     }
// }