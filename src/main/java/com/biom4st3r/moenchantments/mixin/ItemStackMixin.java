package com.biom4st3r.moenchantments.mixin;

import java.util.List;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;
import com.biom4st3r.moenchantments.MoEnchants;
import com.biom4st3r.moenchantments.util;
import com.biom4st3r.moenchantments.Enchantments.MoEnchant;
import com.biom4st3r.moenchantments.interfaces.PotionEffectRetainer;
import com.biom4st3r.moenchantments.interfaces.PotionRetentionTarget;
import com.biom4st3r.moenchantments.interfaces.SentiantEquipment;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.ChatFormat;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CauldronBlock;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

@Mixin(ItemStack.class)
public abstract class ItemStackMixin implements PotionEffectRetainer, SentiantEquipment {


    private static String FRIENDSHIP = "friendship";
    private static String POTIONEFFECT = "potionid";
    private static String POTIONCHARGE = "potioncharge";
    private static String POTIONAMP = "potionamp";

    @Override
    public void talk() 
    {
        
    }
    
    @Override
    public int getMaxCharges()
    {
        return this.isPotionRetainer()*MoEnchantmentsMod.config.perLevelChargeMultiplierForPotionRetention;
    }

    @Override
    public PotionEffectRetainer getRetainer() 
    {
        return (PotionEffectRetainer)(Object)this;
    }

    @Shadow
    public CompoundTag getTag()
    {return new CompoundTag();}
    
    @Override
    public void updateLore() 
    {
        // CompoundTag display = ((ItemStack)(Object)this).getOrCreateSubCompoundTag("display");
        // Component text;

        // if(!(this.getEffect() == null))
        // {
        //     String name = new TranslatableComponent(this.getEffect().getTranslationKey(), new Object[0]).applyFormat(ChatFormat.GRAY).getFormattedText();
        //     text = new TextComponent(String.format("%s %s/%s", name,this.getCharges(),this.getMaxCharges()));
        // }
        // else
        // {
        //     text = new TextComponent("");
        // }
        
        // ListTag lore = new ListTag();
        // lore.add(new StringTag(Serializer.toJsonString(text)));
        // display.put("Lore",lore);
        
    }

    @Override
    public int getFriendship() {
        if(!this.getTag().containsKey(FRIENDSHIP))
        {
            this.getTag().putInt(FRIENDSHIP, 0x00);
        }
        return this.getTag().getInt(FRIENDSHIP);
    }

    @Override
    public void setFriendship(int val) {
        if(val>Byte.MAX_VALUE)
        {
            this.getTag().putInt(FRIENDSHIP, 0xFF);
        }
        else
        {
            this.getTag().putInt(FRIENDSHIP, val);
        }
    }

    @Override
    public void incrementFriendship(int val) {
        int currentVal = this.getTag().getInt(FRIENDSHIP);
        if(!this.getTag().containsKey(FRIENDSHIP))
        {
            this.setFriendship(val);
        }
        else if(currentVal + val > 0xFF)
        {
            this.getTag().putInt(FRIENDSHIP, 0xFF);
        }
        else if(currentVal + val < 0x00)
        {
            this.getTag().putInt(FRIENDSHIP, 0x00);
        }
        else
        {
            this.getTag().putInt(FRIENDSHIP, currentVal + val);
        }
    }



    @Override
    public void setPotionEffectAndCharges(StatusEffectInstance effect, int val) {
        if(this.getEffect() == null)
        {
            this.getTag().putInt(POTIONEFFECT, StatusEffect.getRawId(effect.getEffectType()));
            this.getTag().putInt(POTIONAMP, effect.getAmplifier());
            this.getTag().putInt(POTIONCHARGE, Math.min(val, this.getMaxCharges()));
        }
        else if(this.getEffect() == effect.getEffectType())
        {
            this.addCharges(val);
        }
        else
        {

        }
        ;
    }


    @Override
    public void addCharges(int val) {
        int currentVal = this.getTag().getInt(POTIONCHARGE);
        if(!this.getTag().containsKey(POTIONCHARGE))
        {
            util.Debug("ItemStackMixin#addCharges", "didn't have attributes");
            this.setPotionEffectAndCharges(-1,0,0);
        }
        else if(currentVal + val <= 0x00)
        {
            util.Debug("ItemStackMixin#addCharges","removing effect");
            this.getTag().putInt(POTIONEFFECT, -1);
            this.getTag().putInt(POTIONAMP,-1);
            this.getTag().putInt(POTIONCHARGE, 0x00);
        }
        else if(currentVal + val >= this.getMaxCharges())
        {
            util.Debug("ItemStackMixin#addCharges","Max Charges");
            this.getTag().putInt(POTIONCHARGE, this.getMaxCharges());
        }
        else
        {
            util.Debug("ItemStackMixin#addCharges","changing Charge count");
            this.getTag().putInt(POTIONCHARGE, currentVal + val);
        }
        ;
    }
    @Override
    public void setPotionEffectAndCharges(int effect, int amplifier, int val) {
        setPotionEffectAndCharges(new StatusEffectInstance(StatusEffect.byRawId(effect),0,amplifier),val);
    }

    @Override
    public int getAmplification() {
        return this.getTag().getInt(POTIONAMP);
    }

    @Override
    public StatusEffect getEffect() {
        if(!this.getTag().containsKey(POTIONEFFECT))
        {
            this.getTag().putInt(POTIONEFFECT, -1);
        }
        int effectid = this.getTag().getInt(POTIONEFFECT);
        if(effectid != -1)
        {
            return StatusEffect.byRawId(effectid);
        }
        else
        {
            return null;
        }
    }

    @Override
    public int getCharges() {
        return this.getTag().getInt(POTIONCHARGE);
    }

    @Override
    public boolean isSentiant() {
        return EnchantmentHelper.getLevel(MoEnchants.SENTIENCE, (ItemStack)(Object)this) > 0;
    }

    @Override
    public int isPotionRetainer() {
        return EnchantmentHelper.getLevel(MoEnchants.POTIONRETENTION, (ItemStack)(Object)this);
    }

    @Override
    public StatusEffectInstance useEffect() {
        if(this.getEffect() != null)
        {
            if(this.getCharges()!=0x00)
            {
                StatusEffectInstance temp = new StatusEffectInstance(this.getEffect(),5*20,this.getAmplification());
                addCharges(-1);
                return temp;
            }
        }
        ;
        return null;
    }

    @Inject(at = @At(value = "TAIL"),method = "getTooltipText",cancellable = false)
    @Environment(EnvType.CLIENT)
    public void getTooltipText(PlayerEntity playerEntity_1, TooltipContext tooltipContext_1,CallbackInfoReturnable<List<Component>> ci) 
    {
        
        List<Component> c = ci.getReturnValue();
        if(this.isPotionRetainer() > 0 && ((PotionEffectRetainer) this).getCharges() > 0)
        {
            for(int i = 0; i < c.size(); i++)
            {
                if(c.get(i) instanceof TranslatableComponent)
                {
                    if(((TranslatableComponent)c.get(i)).getKey().contains("potionretension"))
                    {
                        String name = new TranslatableComponent(((PotionEffectRetainer) this).getEffect().getTranslationKey(), new Object[0]).getFormattedText();
                        Component text = new TextComponent(
                            String.format(
                                "%s- %s %s%s %s/%s",
                                ChatFormat.GRAY,
                                name,
                                MoEnchant.toRoman(this.getAmplification()+1),
                                ChatFormat.RESET,
                                ((PotionEffectRetainer) this).getCharges(),
                                ((PotionEffectRetainer) this).getMaxCharges()
                                ));
                        c.add(i+1,text);
                    }
                }
            }
        }
    }


    @Inject(at = @At(value = "RETURN"),method = "useOnBlock")
    public void useOnBlock(ItemUsageContext context,CallbackInfoReturnable<ActionResult> ci) 
    {
        if(this.isPotionRetainer() > 0)
        {
            BlockState cauldron = context.getWorld().getBlockState(context.getBlockPos());
            
            if(cauldron.getBlock() == Blocks.CAULDRON && cauldron.get(CauldronBlock.LEVEL) > 0)
            {
                this.removeEffect();
            }
            
        }
    }
    
    @Override
    public void removeEffect() 
    {
        this.getTag().putInt(POTIONCHARGE, 0);
        this.getTag().putInt(POTIONAMP, -1);
        this.getTag().putInt(POTIONEFFECT, -1);
    }
    
    @Inject(at = @At("RETURN"),method="use",cancellable = true)
    public void use(World world_1, PlayerEntity playerEntity_1, Hand hand_1, CallbackInfoReturnable<TypedActionResult<ItemStack>> ci)
    {
        PotionEffectRetainer usingItem = (PotionEffectRetainer)(Object)playerEntity_1.getStackInHand(hand_1);
        if(usingItem.isPotionRetainer() > 0 && ci.getReturnValue().getResult() != ActionResult.SUCCESS && usingItem.getCharges() > 0 && !playerEntity_1.world.isClient)
        {
            StatusEffectInstance t = usingItem.useEffect();
            t = new StatusEffectInstance(t.getEffectType(),10*20,t.getAmplifier());
            ((PotionRetentionTarget)playerEntity_1).applyRetainedPotionEffect(t);
            ci.setReturnValue(new TypedActionResult<ItemStack>(ActionResult.SUCCESS, (ItemStack)(Object)usingItem));
        }
    }
    
}