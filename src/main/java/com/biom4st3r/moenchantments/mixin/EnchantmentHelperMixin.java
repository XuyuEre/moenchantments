
package com.biom4st3r.moenchantments.mixin;

import java.util.Iterator;
import java.util.List;
import com.biom4st3r.moenchantments.MoEnchants;
import com.biom4st3r.moenchantments.Enchantments.MoEnchant;
import com.google.common.collect.Lists;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import net.minecraft.container.AnvilContainer;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.InfoEnchantment;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.registry.Registry;

@Mixin(EnchantmentHelper.class)
public abstract class EnchantmentHelperMixin {
   /*
    * getHighestAppicableEnchantmentsAtPower return list of all valid enchantments
    * at a given power level
    * 
    */


      /**
       * 
       * @param EnchantmentPower - the amount of bookshelfs around the enchantingtable +- random amount or for Natural Generation just a random amount
       * @param targetItemStack
       * @param isTreasureGeneration - weather or not this is a natural generator of treasure
       * @return
       * 
       */

      

      @Overwrite
      public static List<InfoEnchantment> getHighestApplicableEnchantmentsAtPower(int EnchantmentPower, ItemStack targetItemStack, boolean isTreasureGeneration) 
      {
         List<InfoEnchantment> validEnchants = Lists.newArrayList();
         Item targetItem = targetItemStack.getItem();
         boolean isBook = targetItemStack.getItem() == Items.BOOK;
         Iterator<Enchantment> RegisteredEnchantments = Registry.ENCHANTMENT.iterator();
         
         while(RegisteredEnchantments.hasNext())
         {
            Enchantment currEnchantment = RegisteredEnchantments.next();
            if(!isBook)
            {
               if(currEnchantment instanceof MoEnchant)
               {
                  if(!((MoEnchant)currEnchantment).isAcceptableItem(targetItemStack))
                  {
                     continue;
                  }
               }
               else if(!currEnchantment.type.isAcceptableItem(targetItem))
               {
                  continue;
               }
            }
            if(currEnchantment.isTreasure() && !isTreasureGeneration)
            {
               continue;
            }
            for(int currEnchLvl = currEnchantment.getMaximumLevel(); currEnchLvl >= currEnchantment.getMinimumLevel(); --currEnchLvl) 
            {
               if (EnchantmentPower >= currEnchantment.getMinimumPower(currEnchLvl)) 
               {
                  if(currEnchantment == MoEnchants.ENDERPROTECTION)
                  {
                     validEnchants.add(new InfoEnchantment(currEnchantment, (currEnchLvl == 3 ? 1 : (currEnchLvl == 1 ? 3 : currEnchLvl))));
                  }
                  validEnchants.add(new InfoEnchantment(currEnchantment, currEnchLvl));
                  break;
               }
            }  
         }
         return validEnchants;
      }

   // @Overwrite
   // public static List<InfoEnchantment> getHighestApplicableEnchantmentsAtPower(int EnchantmentPower, ItemStack enchTargetItemStack, boolean isTreasureGeneration) {
   //    List<InfoEnchantment> list_1 = Lists.newArrayList();
   //    Item enchTargetItem = enchTargetItemStack.getItem();
   //    boolean isBook = enchTargetItemStack.getItem() == Items.BOOK;
   //    Iterator RegisteredEnchantments = Registry.ENCHANTMENT.iterator();
   //    int i = 0;
   //    while(true) //only runs once
   //    {
   //       //EnchantingTableContainer
   //       while(true) // runs once per valid enchant on item + 1
   //       {
   //          i++;
   //          Enchantment currEnchantment;

   //          do //isAcceptableItem and !isBook
   //          {
   //             do 
   //             {
   //                if (!RegisteredEnchantments.hasNext()) 
   //                {
   //                   list_1.stream().forEach(((enchant) ->{
   //                   }));
   //                   return list_1;
   //                }

   //                currEnchantment = (Enchantment)RegisteredEnchantments.next();
   //             } while(currEnchantment.isTreasure() && !isTreasureGeneration);

   //          } while(!currEnchantment.type.isAcceptableItem(enchTargetItem) && !isBook);
   //          for(int currentEnchLvl = currEnchantment.getMaximumLevel(); currentEnchLvl >= currEnchantment.getMinimumLevel(); --currentEnchLvl) 
   //          {
   //             if (EnchantmentPower >= currEnchantment.getMinimumPower(currentEnchLvl)) 
   //             {
   //                list_1.add(new InfoEnchantment(currEnchantment, currentEnchLvl));
   //                break;
   //             }
   //          }
   //       }
   //    }
   // }

}
