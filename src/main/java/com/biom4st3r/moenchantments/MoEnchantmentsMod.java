package com.biom4st3r.moenchantments;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.biom4st3r.moenchantments.Items.DummyHeroProjectileItem;
import com.biom4st3r.moenchantments.entities.HerosProjectileEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.netty.buffer.Unpooled;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.entity.FabricEntityTypeBuilder;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityCategory;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.packet.CustomPayloadC2SPacket;
import net.minecraft.util.Identifier;
import net.minecraft.util.PacketByteBuf;
import net.minecraft.util.registry.Registry;

public class MoEnchantmentsMod implements ModInitializer {
	public static final String MODID = "biom4st3rmoenchantments";
	public static MoEnchantsConfig config;
	public static boolean isDebugBuild = false;
	public static EntityType<HerosProjectileEntity> herosProjectile;
	public static Identifier MISSEDATTACK = new Identifier(MoEnchantmentsMod.MODID, "missedattack");
	public static DummyHeroProjectileItem dhpi = new DummyHeroProjectileItem();
	public static final UUID uuidZero = new UUID(0L, 0L);

	public static List<Block> block_whitelist = new ArrayList<Block>();
	public static List<Class> class_whitelist = new ArrayList<Class>();

	@Override
	public void onInitialize() {
		//#region config
		File file = new File(FabricLoader.getInstance().getConfigDirectory().getPath(), "moenchantconfig.json");
		try {
			util.logger("loading config!", true);
			FileReader fr = new FileReader(file);
			config = new Gson().fromJson(fr, MoEnchantsConfig.class);
			FileWriter fw = new FileWriter(file);
			fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
			fw.close();
		} catch (IOException e) {
			util.logger("failed loading! Creating initial config!", true);
			config = new MoEnchantsConfig();
			try {
				FileWriter fw = new FileWriter(file);
				fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
				fw.close();
			} catch (IOException e1) {
				util.logger("failed config!", true);
				e1.printStackTrace();
			}
		}
		//#endregion

		Registry.register(Registry.ITEM, new Identifier("biom4st3r", "heroswipe"), dhpi);
		MoEnchants.init();

		herosProjectile = Registry.register(Registry.ENTITY_TYPE, new Identifier("biom4st3r", "heroswipe"),
				FabricEntityTypeBuilder.create(EntityCategory.MISC, HerosProjectileEntity::new).size(1.5f, 0.5f)
						.build());

		ServerSidePacketRegistry.INSTANCE.register(MISSEDATTACK, (packetContext, packetByteBuf) -> {
			PlayerEntity player = packetContext.getPlayer();
			ItemStack weaponwithhero = player.getMainHandStack();
			if (EnchantmentHelper.getLevel(MoEnchants.HEROSSWORD, weaponwithhero) > 0) {
				debug("MoEnchantmentsMod#onIntialize", "MISSEDATTACK packet summoned heroEntity");
				player.world.spawnEntity(new HerosProjectileEntity(player.world, player, player.x, player.y, player.z));
			}
		});
		whitelistToBlock();
		whitelistToClass();
	}

	public static void whitelistToBlock() {
		for (String s : config.veinMinerBlockWhiteList) {
			Block t = Registry.BLOCK.get(new Identifier(s));
			if (t != Blocks.AIR) {
				util.logger("added " + t.getDropTableId().toString(),true);
				block_whitelist.add(t);
			}
			else
			{
				util.logger(s + " was not found", true);
			}
		}
	}

	public static void whitelistToClass() {
		for (String s : config.veinMinerClassWhiteList) {
			try 
			{
				class_whitelist.add(Class.forName(s));
				util.logger("added " + s, true);
			} catch (ClassNotFoundException e) 
			{
				util.logger(s + " was not found", true);
			}
		}
	}

	public static void debug(String location, Object o)
	{
		if(isDebugBuild)
		{
			System.out.println(location + ": " + o.toString());
		}
		
	}

	public static CustomPayloadC2SPacket createMissPacket()
	{
		PacketByteBuf pbb = new PacketByteBuf(Unpooled.buffer());
		return new CustomPayloadC2SPacket(MISSEDATTACK,pbb);
	}


}