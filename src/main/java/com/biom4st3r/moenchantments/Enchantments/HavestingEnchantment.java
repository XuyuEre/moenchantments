package com.biom4st3r.moenchantments.Enchantments;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.HoeItem;
import net.minecraft.item.ItemStack;

public class HavestingEnchantment extends MoEnchant {

    public HavestingEnchantment()
    {
        super(Weight.UNCOMMON,EnchantmentTarget.DIGGER,new EquipmentSlot[] {EquipmentSlot.MAINHAND});
    }

    @Override
    public String getDisplayName() {
        return "Havester";
    }

    @Override
    public String regName() {
        return "harvester";
    }

    @Override
    public int getMinimumPower(int int_1) {
        return 4*(int_1);
    }

    @Override
    public int getMaximumLevel() {
        return 1;
    }

    @Override
    public int getMinimumLevel() {
        return 1;
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) {
        return iS.getItem() instanceof HoeItem;
    }

    @Override
    public boolean enabled() {
        return false;
    }
    
}