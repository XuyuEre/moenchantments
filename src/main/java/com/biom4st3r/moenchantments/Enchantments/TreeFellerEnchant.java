package com.biom4st3r.moenchantments.Enchantments;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;

public class TreeFellerEnchant extends MoEnchant {

    public TreeFellerEnchant() {
        super(Weight.RARE, EnchantmentTarget.DIGGER, new EquipmentSlot[] { EquipmentSlot.MAINHAND });

    }

    @Override
    public String regName() {
        return "treefeller";
    }

    @Override
    public String getDisplayName() {
        return "Timber";
    }

    @Override
    public int getMaximumLevel() {
        return MoEnchantmentsMod.config.TreeFellerMaxBreakByLvl.length;
    }

    @Override
    public int getMinimumLevel() {
        return 1;
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) {

        return iS.getItem() instanceof AxeItem;
    }

    @Override
    public int getMinimumPower(int level) {
        return level * 5;
    }

    @Override
    public boolean enabled() {
        return MoEnchantmentsMod.config.EnableTreeFeller;
    }


    

}