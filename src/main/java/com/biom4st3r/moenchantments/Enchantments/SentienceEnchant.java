package com.biom4st3r.moenchantments.Enchantments;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;

public class SentienceEnchant extends MoEnchant {

    public SentienceEnchant()
    {
        super(Weight.COMMON, EnchantmentTarget.ALL, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
        //Enchantments
    }
    @Override
    public String getDisplayName() {
        return "Sentience";
    }

    @Override
    public String regName() {
        return "sentience";
    }

    @Override
    public int getMaximumLevel() {
        return 1;
    }

    @Override
    public int getMinimumLevel() {
        return 1;
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) {
        return true;
    }
    
    @Override
    public boolean isTreasure() {
        return true;
    }

    @Override
    public boolean enabled() {
        return false;
    }
    
}