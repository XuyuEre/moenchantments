package com.biom4st3r.moenchantments.Logic;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;
import com.biom4st3r.moenchantments.MoEnchants;
import com.biom4st3r.moenchantments.util;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CropBlock;
import net.minecraft.block.LogBlock;
import net.minecraft.block.OreBlock;
import net.minecraft.block.RedstoneOreBlock;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.BasicInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.RecipeType;
import net.minecraft.recipe.cooking.SmeltingRecipe;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.minecraft.world.chunk.light.ChunkLightingView.Empty;
import net.minecraft.world.loot.context.LootContextParameters;

public class onBreakMixinLogic
{

    public static int cap(int val, int ceiling)
	{
		return val > ceiling ? ceiling : val;
	}

	public static void tryVeinMining(World world,BlockPos blockPos,BlockState blockState,PlayerEntity pe)
    {
		if(world.isClient)
		{
			return;
		}
		ItemStack tool = pe.getMainHandStack();
		if(tool == ItemStack.EMPTY || !tool.hasEnchantments())
		{
			return;
		}
		boolean hasAutoSmelt = false;

		int VeinMinerLvl = EnchantmentHelper.getLevel(MoEnchants.VEINMINER, tool);
		int TreeFellerLvl = EnchantmentHelper.getLevel(MoEnchants.TREEFELLER,tool);
		//int havesterLvl = EnchantmentHelper.getLevel(MoEnchants.HAVESTER,tool);
		hasAutoSmelt = EnchantmentHelper.getLevel(MoEnchants.AUTOSMELT, tool) > 0;

		Block currentType = blockState.getBlock();
		
		int brokenBlocks = 0;
		ServerWorld sWorld = (ServerWorld)world;
		net.minecraft.world.loot.context.LootContext.Builder builder = 
			(new net.minecraft.world.loot.context.LootContext.Builder(sWorld))
				.setRandom(sWorld.random)
				.put(LootContextParameters.POSITION, blockPos)
				.put(LootContextParameters.TOOL, pe.getMainHandStack())
				.put(LootContextParameters.THIS_ENTITY, pe)
				.putNullable(LootContextParameters.BLOCK_ENTITY, (BlockEntity)null);

		List<ItemStack> stacks = blockState.getDroppedStacks(builder);
		Optional<SmeltingRecipe> smeltingResult = pe.world.getRecipeManager().getFirstMatch(RecipeType.SMELTING, new BasicInventory((stacks.size() >0 ? stacks.get(0) : ItemStack.EMPTY)), pe.world);
		
		if(TreeFellerLvl > 0)
        {
            if((currentType instanceof LogBlock))
            {
				brokenBlocks = doVeinMiner(blockState,world,blockPos,MoEnchantmentsMod.config.TreeFellerMaxBreakByLvl[TreeFellerLvl-1],pe,tool,smeltingResult);
				
				if(hasAutoSmelt)
				{
					util.Debug("onBreakMixinLogin#tryVeinMining", brokenBlocks + " blocks broken");
					//dropExperience(brokenBlocks, MoEnchantmentsMod.config.AutoSmeltWoodModifier, blockPos, world);
					world.playSound(pe, blockPos, SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.5F, world.random.nextFloat() * 0.1F + 0.9F);
				}
			}
		}
		else if(VeinMinerLvl > 0)
		{
			if(isValidVeiningBlock(currentType))
			{

				brokenBlocks = doVeinMiner(blockState,world,blockPos,MoEnchantmentsMod.config.VeinMinerMaxBreakByLvl[VeinMinerLvl-1],pe,tool,smeltingResult);
				
				if(hasAutoSmelt)
				{
					util.Debug("onBreakMixinLogin#tryVeinMining", brokenBlocks + " blocks broken");
					world.playSound(pe, blockPos, SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.5F, world.random.nextFloat() * 0.1F + 0.9F);
					//float experianceAutoSmeltModifier = (currentType == Blocks.IRON_ORE ? MoEnchantmentsMod.config.AutoSmeltIronModifier : (currentType == Blocks.GOLD_ORE ? MoEnchantmentsMod.config.AutoSmeltGoldModifier :  0f));
					//dropExperience(brokenBlocks, experianceAutoSmeltModifier, blockPos, world);
				}
			}
		}
		else if(hasAutoSmelt && (isValidVeiningBlock(currentType) || currentType instanceof LogBlock))
		{
			//FIX MEE
			world.playSound(pe, blockPos, SoundEvents.BLOCK_LAVA_EXTINGUISH, SoundCategory.BLOCKS, 0.1F, world.random.nextFloat() * 0.1F + 0.9F);
		}

		
	}

	public static ArrayList<CropBlock> getCropBlocks(World w, BlockPos source, Block type)
	{
		return null;
	}
	
    public static ArrayList<BlockPos> getSameBlocks(World w,BlockPos source, Block type)
    {
        ArrayList<BlockPos> t = new ArrayList<BlockPos>();
		for(BlockPos pos : new BlockPos[] {source.up().south(),source.up().east(),source.up().west(),source.up().north(),
											source.up(),source.down(),source.north(),source.east(),source.south(),source.west(),
											source.down().north(),source.down().east(),source.down().west(),source.down().south()})
        {
            if(w.getBlockState(pos).getBlock() == type)
            {
                t.add(pos);
            }
		}
		
        return t;
	}

	private static boolean isValidVeiningBlock(Block b)
	{
		if(b == Blocks.STONE || b == Blocks.ANDESITE || b == Blocks.GRANITE || b == Blocks.DIORITE)
		{

			return false;
		}
		if(b instanceof OreBlock)
		{
			return true;
		}
		else if(b instanceof RedstoneOreBlock)
		{
			return true;
		}

		else if(MoEnchantmentsMod.class_whitelist.contains(b.getClass()))
		{
			util.Debug("onBreakMixinLogin#isValiVeiningBlock; class_whitelist", b.toString() + " true");
			return true;
		}
		else if(MoEnchantmentsMod.block_whitelist.contains(b))
		{
			util.Debug("onBreakMixinLogin#isValiVeiningBlock; block_whitelist", b.toString() + " true");
			return true;
		}
		util.Debug("onBreakMixinLogin#isValiVeiningBlock", b.toString() + " global false");
		return false;
	}
	
	private static void dropExperience(float experienceValue ,BlockPos blockPos,World world)
	{
		world.spawnEntity(new ExperienceOrbEntity(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), (int)experienceValue));
	}

	private static int doVeinMiner(BlockState blockState,World world,BlockPos blockPos,int maxBlocks,PlayerEntity pe,ItemStack tool, Optional<SmeltingRecipe> recipe)
	{
		util.Debug("onBreakMixinLogic#doVeinMiner", blockState.getBlock());
		ItemStack recipeResult = recipe.isPresent() ? recipe.get().getOutput().copy() : ItemStack.EMPTY;
		int blocksBroken = 0;
		float experiencetotal = 0.0f;
		Queue<BlockPos> toBreak = new LinkedList<BlockPos>();
		toBreak.addAll(getSameBlocks(world,blockPos,blockState.getBlock()));
		while(!toBreak.isEmpty() && blocksBroken <= (maxBlocks) && (MoEnchantmentsMod.config.ProtectItemFromBreaking ? tool.getDurability() > 2 : true))
		{
			BlockPos currPos = toBreak.remove();
			double distance = pe.getBlockPos().getSquaredDistance(new Vec3i(currPos.getX(),currPos.getY(),currPos.getZ()));
			if(Math.sqrt(distance) <= MoEnchantmentsMod.config.MaxDistanceFromPlayer)
			{
				toBreak.addAll(getSameBlocks(world, currPos, blockState.getBlock()));
				if(recipe.isPresent() && EnchantmentHelper.getLevel(MoEnchants.AUTOSMELT, tool) > 0)
				{
					Block.dropStack(world, blockPos, recipeResult.copy());
					experiencetotal += recipe.get().getExperience();
					if(experiencetotal >= 1)
					{
						dropExperience(1f, currPos, world);
						experiencetotal-=1;
					}
				}
				else
				{
					Block.dropStacks(world.getBlockState(currPos), world, pe.getBlockPos(), null, pe, tool);
				}
				world.breakBlock(currPos, false);
				tool.applyDamage(1, pe,(playerEntity_1) -> {
					playerEntity_1.sendToolBreakStatus(pe.getActiveHand());
				});
				blocksBroken++;
			}
		}
		pe.addExhaustion(0.005F * blocksBroken);
		return blocksBroken;
	}
}