package com.biom4st3r.moenchantments;

public class util
{
    public static boolean isDebugBuild = true;

    public static void Debug(String location, Object o)
    {
        if(isDebugBuild)
        {
            System.out.println(location + ": " + o.toString());
        }
    }

    public static void logger(Object o, boolean signature)
    {
        if(signature)
        {
            System.out.println(MoEnchantmentsMod.MODID + ": " + o.toString());
        }
        else
        {
            System.out.println(o.toString());
        }

    }
    


}